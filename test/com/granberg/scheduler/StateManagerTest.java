/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.scheduler;

import java.util.Vector;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Carlo
 */
public class StateManagerTest {

    public StateManagerTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of register method, of class StateManager.
     */
    @Test
    public void testRegister() {
    }

    /**
     * Test of run method, of class StateManager.
     */
    @Test
    public void testRun() {
        ScheduledExecutorService fScheduler = Executors.newScheduledThreadPool(1);
        Vector<ScheduledFuture<?>> scheduleHandlers = new Vector<>();
        StateManager stateManager = new StateManager(scheduleHandlers, fScheduler);
        stateManager.run();
    
    }

    /**
     * Test of propertyChange method, of class StateManager.
     */
    @Test
    public void testPropertyChange() {
    }
}