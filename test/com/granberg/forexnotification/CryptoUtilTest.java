/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.forexnotification;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import org.eclipse.persistence.internal.oxm.conversion.Base64;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Carlo
 */
public class CryptoUtilTest {

    public CryptoUtilTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getInstance method, of class CryptoUtil.
     */
    @Test
    public void testGetInstance() {
    }

    /**
     * Test of encryptPassword method, of class CryptoUtil.
     */
    @Test
    public void testEncryptPassword() {
        try {
            String password = "test";
            KeyGenerator keygen = KeyGenerator.getInstance("DES");
            SecretKey desKey = keygen.generateKey();
            Cipher desCipher;
            desCipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
            desCipher.init(Cipher.ENCRYPT_MODE, desKey);
//            password = appProp.getProperty("password");
            byte[] bytePassword = password.getBytes();
            byte[] byteEncryptedPassword = desCipher.doFinal(bytePassword);
            password = new String(Base64.base64Encode(byteEncryptedPassword));
            System.out.println(password);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException ex) {
            Logger.getLogger(CryptoUtil.class.getName()).log(Level.SEVERE, null, ex);
        }    }

    /**
     * Test of decryptPassword method, of class CryptoUtil.
     */
    @Test
    public void testDecryptPassword() {
    }
   
}