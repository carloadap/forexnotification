/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package forexnotification;

import com.granberg.forexnotification.IService;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Carlo
 */
public class ComponentC implements IService {

    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    public void register(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void start() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                Logger.getLogger(ForexNotificationTest.ComponentA.class.getName()).log(Level.INFO, "running");
                changeSupport.firePropertyChange("COMA", null, "COMPLETED");
            }
        });
        Logger.getLogger(ForexNotificationTest.ComponentA.class.getName()).log(Level.INFO, "started");
        thread.start();
        Logger.getLogger(ForexNotificationTest.ComponentA.class.getName()).log(Level.INFO, "ended");
    }
}
