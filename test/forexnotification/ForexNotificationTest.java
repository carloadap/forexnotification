/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package forexnotification;

import com.granberg.forexnotification.ForexNotification;
import com.granberg.forexnotification.IService;
import com.granberg.IDBAccess;
import com.granberg.MySQLDBAccess;
import com.granberg.forexnotification.AppProperties;
import com.granberg.model.News;
import com.sun.org.apache.xalan.internal.xsltc.runtime.Hashtable;
import com.sun.org.apache.xalan.internal.xsltc.runtime.Parameter;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Properties;
import java.util.Vector;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import samplepackage.DynamicClass;

/**
 *
 * @author Carlo
 */
public class ForexNotificationTest {

    private CountDownLatch startSignal = new CountDownLatch(1);

    public ForexNotificationTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of main method, of class ForexNotification.
     */
    @Test
    public void testMain() throws Exception {
        System.out.println("main");
        String[] args = null;
        ForexNotification.main(args);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    @Test
    public void GetNews() {
        IDBAccess dBAccess = MySQLDBAccess.getInstance();
        List<News> list = dBAccess.findNews();
        for (News news : list) {
            System.out.println(news.getNewsId());
            System.out.println(news.getEvents());
        }
    }

    @Test
    public void OneTimeTask() {
        CountDownLatch startSignal = new CountDownLatch(1);
        ScheduledExecutorService service = Executors.newScheduledThreadPool(1);

        class OneTimeTask implements Runnable {

            @Override
            public void run() {
                Logger.getLogger(OneTimeTask.class.getName()).log(Level.INFO, "This runs for one time only!");
            }
        }

        ScheduledFuture<?> handler = service.schedule(new OneTimeTask(), 1, TimeUnit.SECONDS);
        try {
            startSignal.await();
        } catch (InterruptedException ex) {
            Logger.getLogger(ForexNotificationTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Test
    public void ContinuousTask() {
        CountDownLatch startSignal = new CountDownLatch(1);
        ScheduledExecutorService fScheduler = Executors.newScheduledThreadPool(1);

        class ContinuousTask implements Runnable {

            @Override
            public void run() {
                Logger.getLogger(ContinuousTask.class.getName()).log(Level.INFO, "Continuously Running!");
            }
        }

        ScheduledFuture<?> handler = fScheduler.scheduleWithFixedDelay(new ContinuousTask(), 1, 5, TimeUnit.SECONDS);
        try {
            startSignal.await();
        } catch (InterruptedException ex) {
            Logger.getLogger(ForexNotificationTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Test
    public void CancelTask() {
        ScheduledExecutorService fScheduler = Executors.newScheduledThreadPool(1);

        class ContinuousTask implements Runnable {

            @Override
            public void run() {
                Logger.getLogger(ContinuousTask.class.getName()).log(Level.INFO, "Continuously Running!");
            }
        }
        Vector<ScheduledFuture<?>> scheduleHandlers = new Vector();
        ScheduledFuture<?> handler = fScheduler.scheduleWithFixedDelay(new ContinuousTask(), 1, 5, TimeUnit.SECONDS);
        scheduleHandlers.add(handler);

        System.out.println("Task Size : " + scheduleHandlers.size());

        class StopTask implements Runnable {

            private Vector<ScheduledFuture<?>> SCHEDULE_HANDLERS;

            public StopTask(Vector<ScheduledFuture<?>> scheduleHandlers) {
                this.SCHEDULE_HANDLERS = scheduleHandlers;
            }

            @Override
            public void run() {
                for (ScheduledFuture<?> handler : SCHEDULE_HANDLERS) {
                    handler.cancel(true);
                }
                SCHEDULE_HANDLERS.removeAllElements();
                Logger.getLogger(StopTask.class.getName()).log(Level.INFO, "All handlers is destroyed!");
                Logger.getLogger(StopTask.class.getName()).log(Level.INFO, "Number of Task Running : {0}", String.valueOf(SCHEDULE_HANDLERS.size()));
            }
        }
        fScheduler.schedule(new StopTask(scheduleHandlers), 10, TimeUnit.SECONDS);
        try {
            startSignal.await();
        } catch (InterruptedException ex) {
            Logger.getLogger(ForexNotificationTest.class.getName()).log(Level.SEVERE, null, ex);
        }


    }

    @Test
    public void dynamicInstance() {

        //Declare reflectClass as a DynamicClass
        Class reflectClass = DynamicClass.class;

        //Get class name of DynamicClass
        String className = reflectClass.getName();
        System.out.println(className);

        //Check Class Modifier if
        //isPublic, isPrivate, etc...
        int classModifier = reflectClass.getModifiers();
        System.out.println(Modifier.isPublic(classModifier));

        //Get interfaces in a Class.
        Class[] interfaces = reflectClass.getInterfaces();

        //Get Super class.
        Class classSuper = reflectClass.getSuperclass();
        System.out.println(classSuper.getName());

        //Get Class methods.
        Method[] classMethods = reflectClass.getMethods();
        for (Method method : classMethods) {
            System.out.println(method.getName());
            if (method.getName().startsWith("get")) {
                System.out.println("Getter Method");
            } else if (method.getName().startsWith("set")) {
                System.out.println("Setter Method");
            }

            //Get method return type
            System.out.println("return type : " + method.getReturnType());
            Class[] parameters = method.getParameterTypes();
            System.out.println("Parameter :");
            //Get method Parameters
            for (Class parameter : parameters) {
                System.out.println(parameter.getName());
            }
        }

        //Declare constructor , objec
        Constructor constructor = null;

        Object constructorObject = null;
        try {
            try {
                //Set Constructor reflectClass and set the parameters
                constructor = reflectClass.getConstructor(int.class, String.class);
                //Set Object reflectClass and set the parameters and pass a new instance
                constructorObject = reflectClass.getConstructor(int.class, String.class).newInstance(1, "Carlo");
            } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                Logger.getLogger(ForexNotificationTest.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (NoSuchMethodException | SecurityException ex) {
            Logger.getLogger(ForexNotificationTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        //Create a collection parameters of constructor ( reflectClass )
        Class[] constructParameters = constructor.getParameterTypes();
        //Display parameters of constructor ( reflectClass )
        for (Class class1 : constructParameters) {
            System.out.println("Parameters " + class1.getName());
        }

        //Create a new DynamicClass
        DynamicClass newDynamicClass = null;
        int id = 1;
        String name = "carlo";
        try {
            //Set class equals to DynamicClass and pass parameter id and name
            newDynamicClass = (DynamicClass) constructor.newInstance(id, name);
        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            Logger.getLogger(ForexNotificationTest.class.getName()).log(Level.SEVERE, null, ex);
        }

//        newDynamicClass.getNames();

        //Declare a null Field
        Field privateString = null;
        //Create dynamicClass a new DynamicClass
        DynamicClass dynamicClass = new DynamicClass();

        try {
            //Get a private value of a String inside DynamicClass
            String idString = "id";
            privateString = DynamicClass.class.getDeclaredField(idString);
            //Set the Private String Accessible to true
            privateString.setAccessible(true);
            try {
                //Display private string id.
                String valueOfPrivateString = (String) privateString.get(dynamicClass);
                System.out.println("Private String : " + valueOfPrivateString);
                //Get a private method inside Dynamic Class
                String methodName = "getPrivate";
                Method privateMethod = DynamicClass.class.getDeclaredMethod(methodName);
                //Set the method accsseible to true
                privateMethod.setAccessible(true);
                try {
                    //Set the return value of the private method and display.
                    String getPrivateMethodReturn = (String) privateMethod.invoke(dynamicClass);
                    System.out.println("Private Return Method : " + getPrivateMethodReturn);

                    //Get a private method with parameters in DynamicClass.
                    //Create Class with int and string parameters.
                    Class[] methodParameters = new Class[]{Integer.TYPE, String.class};

                    //Create object and pass a value to the parameters
                    int returnInt = 5;
                    String returnString = "Hello";
                    Object[] parameters = new Object[]{new Integer(returnInt), new String()};

                    //Pass the private method name and the parameters
                    privateMethod = DynamicClass.class.getDeclaredMethod("getOtherPrivate", methodParameters);
                    //Set the private method accessible to true
                    privateMethod.setAccessible(true);
                    //Display the return value of private method with parameters.
                    getPrivateMethodReturn = (String) privateMethod.invoke(dynamicClass, parameters);
                    System.out.println("Return Private Method with Parameters : " + getPrivateMethodReturn);
                } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                    Logger.getLogger(ForexNotificationTest.class.getName()).log(Level.SEVERE, null, ex);
                }
            } catch (IllegalArgumentException | IllegalAccessException | NoSuchMethodException | SecurityException ex) {
                Logger.getLogger(ForexNotificationTest.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (NoSuchFieldException | SecurityException ex) {
            Logger.getLogger(ForexNotificationTest.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Test
    public void hashTable() {
        Hashtable htable = new Hashtable();
        htable.put(1, "test");

        String getValue = (String) htable.get(1);
        Logger.getLogger(ForexNotificationTest.class.getName()).log(Level.INFO, getValue);
    }

    @Test
    public void testPropertyChange() {
        ComponentA componentA = new ComponentA();
        SampleListener listener = new SampleListener();
        componentA.register(listener);
        ComponentB componentB = new ComponentB();
        componentB.register(listener);
        componentA.start();
        componentB.start();
        try {
            startSignal.await();


        } catch (InterruptedException ex) {
            Logger.getLogger(ForexNotificationTest.class
                    .getName()).log(Level.SEVERE, null, ex);
        }


    }

    class SampleListener implements PropertyChangeListener {

        @Override
        public void propertyChange(PropertyChangeEvent pce) {
            if (pce.getPropertyName().equalsIgnoreCase("COMB")) {
                Logger.getLogger(SampleListener.class.getName()).log(Level.INFO, pce.getNewValue().toString());
            }
        }
    }

    class ComponentB {

        private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

        public void register(PropertyChangeListener listener) {
            changeSupport.addPropertyChangeListener(listener);
        }

        public void start() {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    Logger.getLogger(ComponentA.class.getName()).log(Level.INFO, "running");
                    changeSupport.firePropertyChange("COMB", null, "COMPLETED");
                }
            });
            Logger.getLogger(ComponentA.class.getName()).log(Level.INFO, "started");
            thread.start();
            Logger.getLogger(ComponentA.class.getName()).log(Level.INFO, "ended");
        }
    }

    class ComponentA implements IService {

        private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

        public void register(PropertyChangeListener listener) {
            changeSupport.addPropertyChangeListener(listener);
        }

        public void start() {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    Logger.getLogger(ComponentA.class.getName()).log(Level.INFO, "running");
                    changeSupport.firePropertyChange("COMA", null, "COMPLETED");
                }
            });
            Logger.getLogger(ComponentA.class.getName()).log(Level.INFO, "started");
            thread.start();
            Logger.getLogger(ComponentA.class.getName()).log(Level.INFO, "ended");
        }
    }

    @Test
    public void componentHashTable() {
        ComponentA a = new ComponentA();
        Logger.getLogger(ForexNotificationTest.class.getName()).log(Level.INFO, a.getClass().getName());
        String staticName = "forexnotification.ComponentC";
        TaskFactory factory = new TaskFactory();
        IService taskComponent = factory.createTaskComponent(staticName);
        taskComponent.start();
    }

    class TaskFactory {

        public IService createTaskComponent(String name) {
            IService taskComponent = null;
            try {
                Class<?> classObj = Class.forName(name);
                taskComponent = (IService) classObj.newInstance();
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(ForexNotificationTest.class.getName()).log(Level.SEVERE, "{0}", ex.getMessage());
            } catch (InstantiationException ex) {
                Logger.getLogger(ForexNotificationTest.class.getName()).log(Level.SEVERE, "{0}", ex.getMessage());
            } catch (IllegalAccessException ex) {
                Logger.getLogger(ForexNotificationTest.class.getName()).log(Level.SEVERE, "{0}", ex.getMessage());
            }
            return taskComponent;
        }
    }

    @Test
    public void EncryptProperties() {
        Properties prop = new Properties();
        try {
            //set the properties value
            KeyGenerator keygen = KeyGenerator.getInstance("DES");
            SecretKey desKey = keygen.generateKey();
            Cipher desCipher;
            desCipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
            desCipher.init(Cipher.ENCRYPT_MODE, desKey);
            byte[] password = "clsa0185".getBytes("");
            byte[] encryptedPassword = desCipher.doFinal(password);
            String username = "adapcarlo@gmail.com";
            String pass = new String(encryptedPassword);
            prop.setProperty("username", username);
            prop.setProperty("password", pass);

            //save properties to project root folder
            prop.store(new FileOutputStream("config.properties"), null);

        } catch (IOException ex) {
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(ForexNotificationTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeyException ex) {
            Logger.getLogger(ForexNotificationTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchPaddingException ex) {
            Logger.getLogger(ForexNotificationTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalBlockSizeException ex) {
            Logger.getLogger(ForexNotificationTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (BadPaddingException ex) {
            Logger.getLogger(ForexNotificationTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Test
    public void testEncrypt() {
//        CryptoUtil crypto = CryptoUtil.getInstance();
//        String password = crypto.encryptPassword();
//        AppProperties properties = AppProperties.getInstance();
//        properties.load();
//        properties.setProperty("passAppword", password);
//        String username = properties.getProperty("username");
//        String encryptedPassword = properties.getProperty("password");
//        String smtpServer = properties.getProperty("smtpserver");
//        String smtpHost = properties.getProperty("smtphost");
//        String emailTaskClass = properties.getProperty("email.task.class");
//        System.out.println(encryptedPassword);
    }

    @Test
    public void property() {
        AppProperties properties = AppProperties.getInstance();
        properties.load();
        String[] periodicity = properties.getProperty("load").split(",");
        int[] results = new int[periodicity.length];
        for (int i = 0; i < periodicity.length; i++) {
            results[i] = Integer.parseInt(periodicity[i]);
        }
        for (int i : results) {
            System.out.println(i);
        }
    }

    @Test
    public void getNews() {
        IDBAccess dBAccess = MySQLDBAccess.getInstance();
        List<News> list = dBAccess.findNews();
        for (News news : list) {
            Logger.getLogger(ForexNotificationTest.class.getName()).log(Level.SEVERE, "Date : {0}", news.getDate());
            Logger.getLogger(ForexNotificationTest.class.getName()).log(Level.SEVERE, "Currency : {0}", news.getCurrency());
            Logger.getLogger(ForexNotificationTest.class.getName()).log(Level.SEVERE, "Event : {0}", news.getEvents());
            Logger.getLogger(ForexNotificationTest.class.getName()).log(Level.SEVERE, "Actual : {0}", news.getActual());
            Logger.getLogger(ForexNotificationTest.class.getName()).log(Level.SEVERE, "Forecast : {0}", news.getForecast());
            Logger.getLogger(ForexNotificationTest.class.getName()).log(Level.SEVERE, "Previous : {0}", news.getPrevious());
            Logger.getLogger(ForexNotificationTest.class.getName()).log(Level.SEVERE, "Importancee : {0}", news.getImportance());
        }
    }
}