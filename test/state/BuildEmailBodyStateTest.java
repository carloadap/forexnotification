/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package state;

import com.granberg.state.IState;
import com.granberg.state.BuildEmailBodyState;
import com.granberg.state.DataObject;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.logging.Level;
import java.util.logging.Logger;
import ndxlib.mail.EmailMessage;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Carlo
 */
public class BuildEmailBodyStateTest {
    
    public BuildEmailBodyStateTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of next method, of class BuildEmailBodyState.
     */
    @Test
    public void testNext() {
        CountDownLatch startSignal = new CountDownLatch(1);
        System.out.println("next");
        BuildEmailBodyState instance = new BuildEmailBodyState();


        ScheduledExecutorService fScheduler = Executors.newScheduledThreadPool(1);
        Vector<ScheduledFuture<?>> scheduleHandlers = new Vector<>();
        DataObject dataObject = new DataObject();
        dataObject.setFScheduler(fScheduler);
        dataObject.setScheduleHandlers(scheduleHandlers);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date d = dateFormat.parse("2013-07-03 04:30:00");
            dataObject.setNewsDate(d);
            Logger.getLogger(BuildEmailBodyStateTest.class.getName()).log(Level.SEVERE, d.toString());
        } catch (ParseException ex) {
            Logger.getLogger(BuildEmailBodyStateTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
//        dataObject.setNewsDate(2013-07-02 09:32:30);
//        Calendar cal = Calendar.getInstance();
//        cal.setTime(new Date());
//        cal.add(Calendar.SECOND, -3);
//        Date sendDate = cal.getTime();
//        dataObject.setSendDate(sendDate);

        //        EmailMessage emailMessage = new EmailMessage();
//        emailMessage.setFrom("adapcarlo@gmail.com");
//        emailMessage.setRecipients("carloadap@hotmail.com");
//        emailMessage.setBody("test");
//        emailMessage.setSubject("Test");
//        dataObject.setEmailMessage(emailMessage);

        IState expResult = instance.next(dataObject);
        try {
            startSignal.await();
        } catch (InterruptedException ex) {
            Logger.getLogger(SendEmailStateTest.class.getName()).log(Level.SEVERE, null, ex);
        }        
        
        
        
        
        
    }
}