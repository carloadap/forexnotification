/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package NewsScheduler;

import com.granberg.forexnotification.AppProperties;
import com.granberg.forexnotification.CryptoUtil;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import org.eclipse.persistence.internal.oxm.conversion.Base64;
import com.granberg.scheduler.OneTimeScheduleTask;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import com.granberg.scheduler.SendingEmailTask;

/**
 *
 * @author Carlo
 */
public class OneTimeScheduleTaskTest {

    public OneTimeScheduleTaskTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of run method, of class OneTimeScheduleTask.
     */
    @Test
    public void testRun() {
        System.out.println("run");

//        SendingEmailTask emailTask = new SendingEmailTask();
//        emailTask.setEmailInfo(null, null, null, null, null);
//        emailTask.register(new OneTimeScheduleTask(null, null));
//        emailTask.run();

    }

    /**
     * Test of getDistinctNews method, of class OneTimeScheduleTask.
     */
    @Test
    public void testGetDistinctNews() {
    }

    @Test
    public void getProperties() {
        AppProperties properties = AppProperties.getInstance();
        properties.load();
        String username = properties.getProperty("username");
        String password = properties.getProperty("password");
        String smtpServer = properties.getProperty("smtpserver");
        String smtpHost = properties.getProperty("smtphost");
        Logger.getLogger(OneTimeScheduleTaskTest.class.getName()).log(Level.SEVERE, "{0}{1}{2}{3}", new Object[]{username, password, smtpServer, smtpHost});
    }

    @Test
    public void getDecryptedPassword() {
        AppProperties appProp = AppProperties.getInstance();
        appProp.load();
        try {
//            KeyGenerator keygen = KeyGenerator.getInstance("DES");
            SecretKey desKey = new SecretKeySpec("kahitano".getBytes(), "DES");
            Cipher desCipher;
            desCipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
            desCipher.init(Cipher.ENCRYPT_MODE, desKey);
            String password = appProp.getProperty("password");
            System.out.println(password);
            byte[] bytePassword = password.getBytes();
            byte[] byteEncryptedPassword = desCipher.doFinal(bytePassword);
            System.out.println(byteEncryptedPassword);

            String passwordtest = new String(Base64.base64Encode(byteEncryptedPassword));
            System.out.println(passwordtest);
            byte[] password2 = Base64.base64Decode(passwordtest.getBytes());
            System.out.println(password2);
//            String testfinal = new String(password2);
//            System.out.println(testfinal);
            desCipher.init(Cipher.DECRYPT_MODE, desKey);
            byte[] decryptPassword = desCipher.doFinal(password2);
            System.out.println(decryptPassword);
            String decryptString = new String(decryptPassword);
            System.out.println(decryptString);
//            byte[] testByte = password2.getBytes();
////            String finaltest = new String(testByte);
////            System.out.println(finaltest);
//            desCipher.init(Cipher.DECRYPT_MODE, desKey);
//            byte[] byteDescrypt = desCipher.doFinal(testByte);
//            String finalfinal = new String(byteDescrypt);
//            System.out.println(finalfinal);

        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException ex) {
            Logger.getLogger(OneTimeScheduleTaskTest.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Test
    public void decryptEmailPass() {
        try {
//            KeyGenerator keygen = KeyGenerator.getInstance("DES");
            SecretKey desKey = new SecretKeySpec("kahitano".getBytes(), "DES");
            Cipher desCipher;
            desCipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
            AppProperties appProp = AppProperties.getInstance();
            CryptoUtil crypto = CryptoUtil.getInstance();
            appProp.load();
            String password = appProp.getProperty("password");
            System.out.println(password);
            byte[] password2 = Base64.base64Decode(password.getBytes());
            System.out.println(password2);
            desCipher.init(Cipher.DECRYPT_MODE, desKey);
            byte[] decryptPassword = desCipher.doFinal(password2);
            System.out.println(decryptPassword);
            String testfinal = new String(decryptPassword);
            System.out.println(testfinal);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException ex) {
            Logger.getLogger(OneTimeScheduleTaskTest.class.getName()).log(Level.SEVERE, null, ex);
        }


    }

    @Test
    public void testPass() {
        SecretKey desKey = new SecretKeySpec("kahitano".getBytes(), "DES");
        Cipher desCipher;
        try {
            desCipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
            AppProperties appProp = AppProperties.getInstance();
            CryptoUtil crypto = CryptoUtil.getInstance();
            appProp.load();
            String encryptedPassword = appProp.getProperty("password");
            String decripttedPassword = crypto.decryptPassword(encryptedPassword);
            System.out.println(decripttedPassword);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException ex) {
            Logger.getLogger(OneTimeScheduleTaskTest.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}