/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package samplepackage;

/**
 *
 * @author Carlo
 */
public class DynamicClass {

    private String id = "1";

    private String getPrivate() {
        return "How did you get this";
    }

    private String getOtherPrivate(int thisInt, String thatString) {
        return "Return Int :" + thisInt + " Return String : " + thatString;
    }
    
    int Id;
    String Name;
    public DynamicClass(int id, String name) {
//        System.out.println("Your ID is : " + id +  ", Your Name is : " + name);
        this.Id = id;
        this.Name= name;
    }
    
    public DynamicClass(){
        
    }
    
    public void getNames(){
        System.out.println("Carlo Adap");
    }
}
