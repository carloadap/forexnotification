/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.command;

import com.granberg.scheduler.EmailService;

/**
 *
 * @author Carlo
 */
public class Restart implements ICommand {

    @Override
    public void execute() {
        EmailService emailService = EmailService.getInstance();
        emailService.restart();
    }
}
