/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.command;

import com.granberg.forexnotification.AppProperties;
import com.granberg.state.DataObject;

/**
 *
 * @author Carlo
 */
public class Username implements ICommand{

    @Override
    public void execute() {
        AppProperties properties = AppProperties.getInstance();
        DataObject dataObject = DataObject.getInstance();
        properties.load();
        properties.setProperty("username", dataObject.getProperty());
    }
    
}
