/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.command;

import com.granberg.forexnotification.AppProperties;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.granberg.scheduler.SendingEmailTask;

/**
 *
 * @author Carlo
 */
public class EmailOn implements ICommand {

    @Override
    public void execute() {
        AppProperties properties = AppProperties.getInstance();
        properties.load();
        String emailTaskClass = properties.getProperty("email.task.class");
        if (emailTaskClass.matches("scheduler.SendingEmailTask")) {
            Logger.getLogger(EmailOn.class.getName()).log(Level.SEVERE, "News Mailer is already activated.");
        } else {
            properties.setProperty("email.task.class", "scheduler.SendingEmailTask");
            Logger.getLogger(EmailOn.class.getName()).log(Level.SEVERE, "News Mailer is Activated.");
        }
    }
}
