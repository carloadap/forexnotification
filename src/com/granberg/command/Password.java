/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.command;

import com.granberg.forexnotification.AppProperties;
import com.granberg.forexnotification.CryptoUtil;
import com.granberg.state.DataObject;

/**
 *
 * @author Carlo
 */
public class Password implements ICommand{

    @Override
    public void execute() {
        AppProperties properties = AppProperties.getInstance();
        CryptoUtil crypto = CryptoUtil.getInstance();
        DataObject dataObject = DataObject.getInstance();
        properties.load();
        String encryptPassword = crypto.encryptPassword(dataObject.getProperty());
        properties.setProperty("password", encryptPassword);
    }
    
}
