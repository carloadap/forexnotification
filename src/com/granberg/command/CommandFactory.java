/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.command;

import java.util.Hashtable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Carlo
 */
public class CommandFactory {

    private static CommandFactory instance;
    private Hashtable<String, ICommand> commands = new Hashtable<>();

    public static CommandFactory getInstance() {
        if (instance == null) {
            instance = new CommandFactory();
        }
        return instance;
    }

    public ICommand createCommandComponent(String name) {
        ICommand taskComponent = commands.get(name);
        try {
            if (taskComponent == null) {
                Class<?> classObj = Class.forName(name);
                taskComponent = (ICommand) classObj.newInstance();
                commands.put(name, taskComponent);
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(CommandFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        return taskComponent;
    }
}
