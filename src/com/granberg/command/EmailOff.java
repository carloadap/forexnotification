/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.command;

import com.granberg.forexnotification.AppProperties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Carlo
 */
public class EmailOff implements ICommand {

    @Override
    public void execute() {
        AppProperties properties = AppProperties.getInstance();
        properties.load();
        String emailTaskClass = properties.getProperty("email.task.class");
        if (emailTaskClass.matches("scheduler.SendingWithoutEmailTask")) {
            Logger.getLogger(EmailOff.class.getName()).log(Level.SEVERE, "News Mailer is already Deactivated.");
        } else {
            properties.setProperty("email.task.class", "scheduler.SendingWithoutEmailTask");
            Logger.getLogger(EmailOff.class.getName()).log(Level.SEVERE, "News Mailer is Deactivated.");
        }
    }
}
