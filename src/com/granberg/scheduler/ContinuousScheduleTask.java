/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.scheduler;

import com.granberg.forexnotification.IService;
import java.util.Vector;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Carlo
 */
public class ContinuousScheduleTask implements Runnable {

    Vector<ScheduledFuture<?>> ScheduleHandlers;
    ScheduledExecutorService FScheduler;

    public ContinuousScheduleTask(Vector<ScheduledFuture<?>> scheduleHandlers, ScheduledExecutorService fScheduler) {
        this.ScheduleHandlers = scheduleHandlers;
        this.FScheduler = fScheduler;
    }

    /**
     * Cancel Existing Handlers. Delete all the handlers in Vector. Re run the
     * Automated News Mailer.
     */
    @Override
    public void run() {
        for (ScheduledFuture<?> scheduledFuture : ScheduleHandlers) {
            scheduledFuture.cancel(true);
        }
        ScheduleHandlers.removeAllElements();
        Logger.getLogger(ContinuousScheduleTask.class.getName()).log(Level.SEVERE, "Running Task : {0}", String.valueOf(ScheduleHandlers.size()));
        ScheduledFuture<?> handler = FScheduler.schedule(new OneTimeScheduleTask(ScheduleHandlers, FScheduler), 1, TimeUnit.SECONDS);
        ScheduleHandlers.add(handler);
    }
}
