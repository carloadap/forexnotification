/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.scheduler;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.logging.Level;
import java.util.logging.Logger;
import ndxlib.mail.EmailMessage;
import ndxlib.mail.SendMail;

/**
 * Send email to active recipients.
 */
public class SendingWithoutEmailTask implements Runnable, IEmailTask {

    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);
    String Username, Password, SmtpServer, SmtpHost;
    EmailMessage Message;

    public SendingWithoutEmailTask() {
    }

    public void setEmailInfo(String username, String password, String smtpServer, String smtpHost, EmailMessage emailMessage) {
        this.Username = username;
        this.Password = password;
        this.SmtpServer = smtpServer;
        this.SmtpHost = smtpHost;
        this.Message = emailMessage;
    }

    public void register(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    @Override
    public void run() {
        try {
            Logger.getLogger(SendingEmailTask.class.getName()).log(Level.SEVERE, "Email Sent Successfully");
        } catch (Exception ex) {
            Logger.getLogger(SendingWithoutEmailTask.class.getName()).log(Level.WARNING, "Error Sending Email!{0}", ex);
        }
    }
}