/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.scheduler;

import com.granberg.model.News;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.json.JSONJAXBContext;
import com.sun.jersey.api.json.JSONMarshaller;
import java.io.StringWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBException;

/**
 *
 * @author Carlo
 */
public class SendingJsonTask implements Runnable {

    private List<News> news;

    public SendingJsonTask(List<News> news) {
        this.news = news;
    }

    @Override
    public void run() {
        try {
            for (News json : news) {
                Client client = Client.create();
                WebResource resource = client.resource("http://localhost:8080/NadexWS/webresources/news");
//                WebResource resource = client.resource("http://tradingws.kyoobiq.com:8080/NadexWS/webresources/news");
                JSONJAXBContext ctx = new JSONJAXBContext(News.class);
                JSONMarshaller jsonM = ctx.createJSONMarshaller();
                StringWriter writer = new StringWriter();
                jsonM.marshallToJSON(json, writer);
                System.out.println(writer);
                resource.path("notifyInstrument").accept(MediaType.APPLICATION_JSON).entity(writer.toString(), MediaType.APPLICATION_JSON).post();
                Logger.getLogger(SendingJsonTask.class.getName()).log(Level.SEVERE, "JSON Sent");
            }

        } catch (JAXBException ex) {
            Logger.getLogger(SendingJsonTask.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
