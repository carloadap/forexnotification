/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.scheduler;

import com.granberg.IDBAccess;
import com.granberg.MySQLDBAccess;
import com.granberg.model.DelayValue;
import com.granberg.model.News;
import com.granberg.model.NewsRecipient;
import com.granberg.forexnotification.AppProperties;
import com.granberg.forexnotification.CryptoUtil;
import com.granberg.forexnotification.TaskFactory;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import ndxlib.mail.EmailMessage;

/**
 *
 * @author Carlo
 */
public class OneTimeScheduleTask implements Runnable, PropertyChangeListener {

    Vector<ScheduledFuture<?>> ScheduleHandlers;
    ScheduledExecutorService FScheduler;
    IDBAccess dBAccess = MySQLDBAccess.getInstance();

    public OneTimeScheduleTask(Vector<ScheduledFuture<?>> scheduleHandlers, ScheduledExecutorService fScheduler) {
        this.ScheduleHandlers = scheduleHandlers;
        this.FScheduler = fScheduler;
    }

    /**
     * Automated News Mailer.
     */
    @Override
    public void run() {
        getDistinctNews();
    }

    /**
     * Get distinct news date from the database.
     */
    public void getDistinctNews() {
        List<Date> distinctNewsDate = dBAccess.findDistinctNews();

        for (Date date : distinctNewsDate) {
            Logger.getLogger(OneTimeScheduleTask.class.getName()).log(Level.INFO, "Distinct date : {0}", date);
            processSameNewsFromDistinctNews(date);
        }
    }

    /**
     * Process news with the same date from distinct news. Avoiding currencies
     * and events to be duplicated.
     *
     * @param newsDate
     */
    private void processSameNewsFromDistinctNews(Date newsDate) {
        DelayValue delayValue = dBAccess.findDelayValue(1);
        Calendar cal = Calendar.getInstance();
        cal.setTime(newsDate);
        cal.add(Calendar.MINUTE, -delayValue.getDelayValue());
        Date sendDate = cal.getTime();

        Date today = new Date();
        if (sendDate.compareTo(today) > 0) {
            List<News> list = dBAccess.findSameDateTimeNews(newsDate);
            String currency = "";
            String event = "";
            for (News news : list) {
                if (!currency.contains(news.getCurrency())) {
                    currency = currency + news.getCurrency() + ", ";
                }
                if (!event.contains(news.getEvents())) {
                    event = event + news.getEvents() + ", ";
                }
            }
            String body = String.format("Currencies Affected : %s\n Event Name/s : %s\n Event Date : %s", currency, event, newsDate.toString());
            StringBuilder recipientNames = getRecipients();
            createAndSendEmail(body, recipientNames, sendDate);
        }
    }

    /**
     * Generating email.
     *
     * @param body
     * @param recipientNames
     * @param sendDate
     */
    private void createAndSendEmail(String body, StringBuilder recipientNames, Date sendDate) {
        Logger.getLogger(OneTimeScheduleTask.class.getName()).log(Level.INFO, "Recipients : {0}", recipientNames.toString());
        Logger.getLogger(OneTimeScheduleTask.class.getName()).log(Level.INFO, "Generating Email. . . .");
        EmailMessage emailMessage = new EmailMessage();
        emailMessage.setFrom("Automated Service<adapcarlo@gmail.com>");
        emailMessage.setRecipients(recipientNames.toString());
        emailMessage.setSubject("Upcoming Forex News");
        emailMessage.setBody(body);
        Logger.getLogger(OneTimeScheduleTask.class.getName()).log(Level.INFO, "Email Constructed.");
        long emailSendDateTimeLapse = sendDate.getTime() - System.currentTimeMillis();
        AppProperties properties = AppProperties.getInstance();
        CryptoUtil crypto = CryptoUtil.getInstance();
        properties.load();
        String username = properties.getProperty("username");
        String encryptedPassword = properties.getProperty("password");
        String decryptedPassword = crypto.decryptPassword(encryptedPassword);
        String smtpServer = properties.getProperty("smtpserver");
        String smtpHost = properties.getProperty("smtphost");
        String emailTaskClass = properties.getProperty("email.task.class");
//        SendingEmailTask emailTask = new SendingEmailTask(username, password, smtpServer, smtpHost, emailMessage);
//        emailTask.register(this);
        IEmailTask emailTask = (IEmailTask) TaskFactory.getInstance().createTaskComponent(emailTaskClass);
        emailTask.setEmailInfo(username, decryptedPassword, smtpServer, smtpHost, emailMessage);
        emailTask.register(this);
        ScheduledFuture<?> handler = FScheduler.schedule((Runnable) emailTask, emailSendDateTimeLapse, TimeUnit.MILLISECONDS);
        ScheduleHandlers.add(handler);
    }

    /**
     * Get active recipients.
     *
     * @return
     */
    private StringBuilder getRecipients() {
        List<NewsRecipient> recipientList = dBAccess.findActiveRecipients(true);
        StringBuilder recipientNames = new StringBuilder();
        for (int i = 0; i < recipientList.size(); i++) {
            NewsRecipient newsRecipient = recipientList.get(i);
            if (i != recipientList.size() - 1) {
                recipientNames.append(String.format("%s %s <%s>,", newsRecipient.getFirstName(), newsRecipient.getLastName(), newsRecipient.getEmailAddress()));
            } else {
                recipientNames.append(String.format("%s %s <%s>", newsRecipient.getFirstName(), newsRecipient.getLastName(), newsRecipient.getEmailAddress()));
            }
        }
        return recipientNames;
    }

    @Override
    public void propertyChange(PropertyChangeEvent pce) {
        Logger.getLogger(OneTimeScheduleTask.class.getName()).log(Level.SEVERE, pce.getNewValue().toString());
    }
}
