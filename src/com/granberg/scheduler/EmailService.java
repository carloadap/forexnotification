/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.scheduler;

import com.granberg.forexnotification.IService;
import com.granberg.forexnotification.TaskFactory;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Vector;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.charset.Charset;
import java.util.Stack;

import org.apache.mina.core.service.IoAcceptor;
import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.filter.codec.textline.TextLineCodecFactory;
import org.apache.mina.filter.logging.LoggingFilter;
import org.apache.mina.transport.socket.nio.NioSocketAcceptor;

/**
 *
 * @author Carlo
 */
public class EmailService extends IoHandlerAdapter implements IService {

    ScheduledExecutorService fScheduler = Executors.newScheduledThreadPool(1);
    Vector<ScheduledFuture<?>> scheduleHandlers = new Vector<>();
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);
    String[] x = new String[]{"Toby"};
    private static final int PORT = 9125;
    private Stack<PropertyChangeListener> stack = new Stack<>();
    private IoAcceptor acceptor = new NioSocketAcceptor();
    private static EmailService instance;

    public static EmailService getInstance() {
        if (instance == null) {
            instance = new EmailService();
        }
        return instance;
    }

    public void register(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    /**
     * Email Service
     */
    @Override
    public void start() {
        try {
            startMinaServer();
        } catch (IOException ex) {
            Logger.getLogger(EmailService.class.getName()).log(Level.SEVERE, null, ex);
        }
        TaskFactory taskFactory = TaskFactory.getInstance();
        String staticTaskName = StateManager.class.getName();
        Runnable stateManager = taskFactory.createTaskComponent(staticTaskName, scheduleHandlers, fScheduler);
        fScheduler.scheduleWithFixedDelay(stateManager, 1, 86400, TimeUnit.SECONDS);
        Logger.getLogger(EmailService.class.getName()).log(Level.SEVERE, "Email Service Started");
        System.out.println("start" + x + ":" + changeSupport.getPropertyChangeListeners().length);
    }

    private void startMinaServer() throws IOException {
        Logger.getLogger(EmailService.class.getName()).log(Level.SEVERE, "Mina Server Started");

        if (acceptor == null) {
            acceptor = new NioSocketAcceptor();
        }

        if (!acceptor.isActive()) {
            acceptor.getFilterChain().addLast("logger", new LoggingFilter());
            acceptor.getFilterChain().addLast("codec", new ProtocolCodecFilter(new TextLineCodecFactory(Charset.forName("UTF-8"))));
            acceptor.setHandler(this);
            acceptor.getSessionConfig().setReadBufferSize(2048);
            acceptor.getSessionConfig().setIdleTime(IdleStatus.BOTH_IDLE, 10);

            acceptor.bind(new InetSocketAddress(PORT));
            System.out.println("sns reg" + changeSupport.hashCode() + ":" + changeSupport.getPropertyChangeListeners().length);
        }
    }

    @Override
    public void messageReceived(IoSession session, Object message) {
        changeSupport.firePropertyChange("EmailService", null, message.toString());
        System.out.println(message.toString());
        System.out.println("Message written...");
    }

    public void restart() {
        start();
    }
}
