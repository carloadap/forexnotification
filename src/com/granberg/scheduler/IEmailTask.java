/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.scheduler;

import java.beans.PropertyChangeListener;
import ndxlib.mail.EmailMessage;

/**
 *
 * @author Carlo
 */
public interface IEmailTask {
    
     public void setEmailInfo(String username, String password, String smtpServer, String smtpHost, EmailMessage emailMessage);
     public void register(PropertyChangeListener listener);
}
