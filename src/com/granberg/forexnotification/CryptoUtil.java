/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.forexnotification;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import org.eclipse.persistence.internal.oxm.conversion.Base64;

/**
 *
 * @author Carlo
 */
public class CryptoUtil {

    private static CryptoUtil instance;

    public static CryptoUtil getInstance() {
        if (instance == null) {
            instance = new CryptoUtil();
        }
        return instance;
    }

    public String encryptPassword(String password) {
//        String password = null;
//        AppProperties appProp = AppProperties.getInstance();
//        appProp.load();
        try {
            KeyGenerator keygen = KeyGenerator.getInstance("DES");
            SecretKey desKey = keygen.generateKey();
            Cipher desCipher;
            desCipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
            desCipher.init(Cipher.ENCRYPT_MODE, desKey);
//            password = appProp.getProperty("password");
            byte[] bytePassword = password.getBytes();
            byte[] byteEncryptedPassword = desCipher.doFinal(bytePassword);
            password = new String(Base64.base64Encode(byteEncryptedPassword));
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException ex) {
            Logger.getLogger(CryptoUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        return password;
    }

    public String decryptPassword(String encryptedPassword) {
        String decryptedPassword = null;
        try {
//            KeyGenerator keygen = KeyGenerator.getInstance("DES");
            SecretKey desKey = new SecretKeySpec("kahitano".getBytes(), "DES");
            Cipher desCipher;
            desCipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
            byte[] password = Base64.base64Decode(encryptedPassword.getBytes());
//            System.out.println(password);
            desCipher.init(Cipher.DECRYPT_MODE, desKey);
            byte[] bytePassword = desCipher.doFinal(password);
            decryptedPassword = new String(bytePassword);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException ex) {
            Logger.getLogger(CryptoUtil.class.getName()).log(Level.SEVERE, null, ex);
        }

//            byte[] decryptBytePass = desCipher.doFinal(encrypBytePass);
//            decryptedPassword = new String(decryptBytePass);

        return decryptedPassword;
    }
}
