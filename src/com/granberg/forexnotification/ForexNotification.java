/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.forexnotification;

import com.granberg.command.CommandFactory;
import com.granberg.command.ICommand;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import com.granberg.scheduler.EmailService;
import com.granberg.state.DataObject;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Carlo
 */
public class ForexNotification {

    /**
     * @param args the command line arguments
     *
     */
    public static void main(String[] args) throws IOException {

        EmailService newsService = EmailService.getInstance();
        newsService.register(new ChangeDynamicListener());
        newsService.start();
        System.in.read();
        System.exit(0);
        Logger.getLogger(ForexNotification.class.getName()).log(Level.INFO, newsService.getClass().getName());
    }

    private static class ChangeListener implements PropertyChangeListener {

        Commands command = Commands.getInstance();

        @Override
        public void propertyChange(PropertyChangeEvent pce) {
            Logger.getLogger(ChangeListener.class.getName()).log(Level.SEVERE, "im here");
            if (pce.getNewValue().toString().equalsIgnoreCase("quit")) {
                command.quit();
            } else if (pce.getNewValue().toString().equalsIgnoreCase("offemail")) {
                command.emailOff();
            } else if (pce.getNewValue().toString().equalsIgnoreCase("onemail")) {
                command.emailOn();
            } else if (pce.getNewValue().toString().startsWith("username")) {
                command.changeUserName(pce.getNewValue().toString());
            } else if (pce.getNewValue().toString().startsWith("password")) {
                command.changePassword(pce.getNewValue().toString());
            } else if (pce.getNewValue().toString().equalsIgnoreCase("restart")) {
                command.restart();
            }
        }
    }

    private static class ChangeDynamicListener implements PropertyChangeListener {

        Commands command = Commands.getInstance();

        @Override
        public void propertyChange(PropertyChangeEvent pce) {
            Logger.getLogger(ChangeListener.class.getName()).log(Level.SEVERE, "New");
            DataObject dataObject = DataObject.getInstance();
            String value[] = pce.getNewValue().toString().split(" ");
            String newCommand = value[0];
            if (value.length > 1) {
                dataObject.setProperty(value[1]);
            }
            String cmdName = String.format("%s.%s", CommandFactory.class.getPackage().getName(), newCommand);
            ICommand command = CommandFactory.getInstance().createCommandComponent(cmdName);
//             command.setParameter(); JSON parameter
            command.execute();
        }
    }
}
