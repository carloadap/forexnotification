/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.forexnotification;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Carlo
 */
class EmailOn implements ICommands {

    Commands command;

    public EmailOn(Commands newCommand) {
        command = newCommand;
    }

    @Override
    public void emailOn() {
        Logger.getLogger(EmailOn.class.getName()).log(Level.SEVERE, "Forex News Mailer is already activated.");
    }

    @Override
    public void emailOff() {
    }
}
