/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.forexnotification;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Hashtable;
import java.util.Vector;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Carlo
 */
public class TaskFactory {

    private static TaskFactory instance;
    private Hashtable<String, Runnable> tasks = new Hashtable<>();

    public static TaskFactory getInstance() {
        if (instance == null) {
            instance = new TaskFactory();
        }
        return instance;
    }

    public Runnable createTaskComponent(String name) {
        Runnable taskComponent = tasks.get(name);
        try {
            if (taskComponent == null) {
                Class<?> classObj = Class.forName(name);
                taskComponent = (Runnable) classObj.newInstance();
                tasks.put(name, taskComponent);
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(TaskFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        return taskComponent;
    }

    public Runnable createTaskComponent(String name, Vector<ScheduledFuture<?>> schedulehandler, ScheduledExecutorService fscheduler) {
        Runnable taskComponent = tasks.get(name);
        try {
            if (taskComponent == null) {
                Class classObj = Class.forName(name);
                Constructor constructor = classObj.getConstructor(Vector.class, ScheduledExecutorService.class);
                taskComponent = (Runnable) constructor.newInstance(schedulehandler, fscheduler);
                tasks.put(name, taskComponent);
            }
        } catch (ClassNotFoundException | NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            Logger.getLogger(TaskFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        return taskComponent;
    }
}
