/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.forexnotification;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Carlo
 */
class EmailOff implements ICommands {

    Commands command;

    public EmailOff(Commands newCommand) {
        command = newCommand;
    }

    @Override
    public void emailOn() {
        AppProperties properties = AppProperties.getInstance();
        properties.load();
        properties.setProperty("email.task.class", "scheduler.SendingEmailTask");
        command.setCommandState(command.getEmailOnState());
        Logger.getLogger(EmailOff.class.getName()).log(Level.SEVERE, "Forex News Mailer is now activated.");
    }

    @Override
    public void emailOff() {
        Logger.getLogger(EmailOff.class.getName()).log(Level.SEVERE, "Forex News Mailer is already disabled.");
    }
}
