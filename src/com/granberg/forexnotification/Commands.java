/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.forexnotification;

import javax.mail.Session;
import com.granberg.scheduler.EmailService;

/**
 *
 * @author Carlo
 */
public class Commands {

    private static Commands instance;

    public static Commands getInstance() {
        if (instance == null) {
            instance = new Commands();
        }
        return instance;
    }
    ICommands onEmail;
    ICommands offEmail;
    ICommands commandState;

    public Commands() {
        onEmail = new EmailOn(this);
        offEmail = new EmailOff(this);
        commandState = onEmail;
    }

    void setCommandState(ICommands newICommandState) {
        commandState = newICommandState;
    }

    public void quit() {
        System.exit(0);
    }
    
    public void restart(){
        EmailService emailService = EmailService.getInstance();
        emailService.restart();
    }

    public void changeUserName(String newUsername) {
        AppProperties properties = AppProperties.getInstance();
        properties.load();
        String[] value = newUsername.split(" ");
        String setUsername = value[1];
//        properties.setProperty("username", setUsername);
    }

    public void changePassword(String newPassowrd) {
        AppProperties properties = AppProperties.getInstance();
        CryptoUtil crypto = CryptoUtil.getInstance();
        properties.load();
        String[] value = newPassowrd.split(" ");
        String setPassword = crypto.encryptPassword(value[1]);
//        properties.setProperty("password", setPassword);
    }

    public void emailOn() {
        commandState.emailOn();
    }

    public void emailOff() {
        commandState.emailOff();
    }

    public ICommands getEmailOnState() {
        return onEmail;
    }

    public ICommands getEmailOffState() {
        return offEmail;
    }
}
