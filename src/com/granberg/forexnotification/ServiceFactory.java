/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.forexnotification;

import java.util.Hashtable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Carlo
 */
public class ServiceFactory {

    private static ServiceFactory instance;
    private Hashtable<String, IService> tasks = new Hashtable<>();

    public static ServiceFactory getInstance() {
        if (instance == null) {
            instance = new ServiceFactory();
        }
        return instance;
    }

    public IService createServiceComponent(String name) {
        IService taskComponent = tasks.get(name);
        try {
            if (taskComponent == null) {
                Class<?> classObj = Class.forName(name);
                taskComponent = (IService) classObj.newInstance();
                tasks.put(name, taskComponent);
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(ServiceFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        return taskComponent;
    }
}