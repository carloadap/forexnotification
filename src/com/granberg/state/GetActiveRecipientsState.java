/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.state;

import com.granberg.model.NewsRecipient;
import com.granberg.forexnotification.AppProperties;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Carlo
 */
public class GetActiveRecipientsState extends AbstractState {

    @Override
    public IState next(DataObject dataObject) {
        IState retval = null;
        Logger.getLogger(GetActiveRecipientsState.class.getName()).log(Level.INFO, "Get Active Recipients called!");
        StringBuilder recipientNames = getRecipients();

//        dataObject.getEmailMessage().setRecipients("carloadap@hotmail.com");

//        dataObject.setRecipients("carloadap@hotmail.com");

        dataObject.getEmailMessage().setRecipients(recipientNames.toString());
//        recipientNames.toString()

//        AppProperties properties = AppProperties.getInstance();
//        properties.load();
//        String username = properties.getProperty("username");
//        dataObject.getEmailMessage().setFrom(username);

//        dataObject.getEmailMessage().setFrom("adapcarlo@gmail.com");

//        dataObject.getEmailMessage().setSubject("Upcoming Forex News");

//        createAndSendEmail(body, recipientNames, sendDate);
        retval = StateFactory.getInstance().createStateComponent(SendEmailState.class.getName());
        return retval;
    }

    private StringBuilder getRecipients() {
        List<NewsRecipient> recipientList = dBAccess.findActiveRecipients(true);
        StringBuilder recipientNames = new StringBuilder();
        for (int i = 0; i < recipientList.size(); i++) {
            NewsRecipient newsRecipient = recipientList.get(i);
            if (i != recipientList.size() - 1) {
                recipientNames.append(String.format("%s %s <%s>,", newsRecipient.getFirstName(), newsRecipient.getLastName(), newsRecipient.getEmailAddress()));
            } else {
                recipientNames.append(String.format("%s %s <%s>", newsRecipient.getFirstName(), newsRecipient.getLastName(), newsRecipient.getEmailAddress()));
            }
        }
        return recipientNames;
    }
}
