/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.state;

import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Carlo
 */
public class GetDistinctNewsDateState extends AbstractState {

    @Override
    public IState next(DataObject dataObject) {
        IState retval = null;
        Logger.getLogger(GetDistinctNewsDateState.class.getName()).log(Level.INFO, "Distinct News Date called!");
        List<Date> distinctNewsDate = dBAccess.findDistinctNews();
        dataObject.setDistinctNewsDate(distinctNewsDate);
        retval = StateFactory.getInstance().createStateComponent(GetNewsState.class.getName());
        return retval;
    }
}
