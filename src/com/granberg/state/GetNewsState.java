/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.state;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.granberg.scheduler.StateManager;

/**
 *
 * @author Carlo
 */
public class GetNewsState extends AbstractState {

    @Override
    public IState next(DataObject dataObject) {
        IState retval = null;
        Logger.getLogger(GetNewsState.class.getName()).log(Level.INFO, "News Date called!");
        for (Date date : dataObject.getDistinctNewsDate()) {
            IState state = StateFactory.getInstance().createStateComponent(BuildEmailBodyState.class.getName());
            dataObject.setNewsDate(date);
            Logger.getLogger(StateManager.class.getName()).log(Level.INFO, state.toString());
            while (state != null) {
                Logger.getLogger(GetNewsState.class.getName()).log(Level.INFO, "NEWS DATE : {0}", date.toString());
                state = state.next(dataObject);
            }
        }
        return retval;
    }
}
