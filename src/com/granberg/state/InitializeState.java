/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.state;

import java.util.concurrent.ScheduledFuture;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Carlo
 */
public class InitializeState extends AbstractState {

    @Override
    public IState next(DataObject dataObject) {
        IState retval = null;
        Logger.getLogger(InitializeState.class.getName()).log(Level.INFO, "Initialize called!");
        for (ScheduledFuture<?> scheduledFuture : dataObject.getScheduleHandlers()) {
            scheduledFuture.cancel(true);
        }
        dataObject.getScheduleHandlers().removeAllElements();
        retval = StateFactory.getInstance().createStateComponent(GetDistinctNewsDateState.class.getName());
        return retval;
    }
}
