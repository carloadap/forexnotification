/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.state;

import com.granberg.model.News;
import com.granberg.scheduler.SendingJsonTask;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.json.JSONJAXBContext;
import com.sun.jersey.api.json.JSONMarshaller;
import java.io.StringWriter;
import java.util.List;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBException;

/**
 *
 * @author Carlo
 */
public class SendJsonState extends AbstractState {

    @Override
    public IState next(DataObject dataObject) {
        IState retval = null;
        List<News> jsonNewsList = dataObject.getNewsList();
        Logger.getLogger(SendJsonState.class.getName()).log(Level.INFO, "News Notification");
        SendingJsonTask jsonTask = new SendingJsonTask(jsonNewsList);
        ScheduledFuture<?> handler = dataObject.getFScheduler().schedule((Runnable) jsonTask, dataObject.getSendTimeLapse(), TimeUnit.MILLISECONDS);
        return retval;
    }
}