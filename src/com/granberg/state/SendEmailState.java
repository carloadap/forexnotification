/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.state;

import com.granberg.forexnotification.AppProperties;
import com.granberg.forexnotification.CryptoUtil;
import com.granberg.model.News;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.granberg.scheduler.SendingEmailTask;
import com.granberg.scheduler.SendingJsonTask;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Carlo
 */
public class SendEmailState extends AbstractState {

    @Override
    public IState next(DataObject dataObject) {
        IState retval = null;
        Logger.getLogger(SendEmailState.class.getName()).log(Level.INFO, "Send Email called!");
        AppProperties properties = AppProperties.getInstance();
        CryptoUtil crypto = CryptoUtil.getInstance();
        properties.load();
        String encryptedPassword = properties.getProperty("password");
        String decryptedPassword = crypto.decryptPassword(encryptedPassword);
        String username = properties.getProperty("username");
        String smtpServer = properties.getProperty("smtpserver");
        String smtpHost = properties.getProperty("smtphost");
        String emailTaskClass = properties.getProperty("email.task.class");
//        EmailMessage emailMessage = new EmailMessage();
//        emailMessage.setFrom("Automated Service <" + username + ">");
//        emailMessage.setRecipients(dataObject.getRecipients());
//        emailMessage.setSubject(dataObject.getSubject());
//        emailMessage.setBody(dataObject.getBody());
        dataObject.getEmailMessage().setFrom("Automated Service <" + username + ">");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dataObject.getSendDate());
        calendar.add(Calendar.HOUR_OF_DAY, -1);
        Date send = calendar.getTime();
        long emailSendDateTimeLapse = send.getTime() - System.currentTimeMillis();
        dataObject.setSendTimeLapse(emailSendDateTimeLapse);
//        IEmailTask emailTask = (IEmailTask) TaskFactory.getInstance().createTaskComponent(emailTaskClass);
//        emailTask.setEmailInfo(username, decryptedPassword, smtpServer, smtpHost, emailMessage);
        SendingEmailTask emailTask = new SendingEmailTask(username, decryptedPassword, smtpServer, smtpHost, dataObject.getEmailMessage(),dataObject.getNews());
//        SendingJsonTask jsonTask = new SendingJsonTask(dataObject.getNews());
        if (dataObject.getFScheduler() != null) {
            ScheduledFuture<?> handler = dataObject.getFScheduler().schedule((Runnable) emailTask, emailSendDateTimeLapse, TimeUnit.MILLISECONDS);
//            dataObject.getFScheduler().schedule((Runnable)jsonTask, emailSendDateTimeLapse, TimeUnit.MILLISECONDS);
            dataObject.getScheduleHandlers().add(handler);
        } else {
            Logger.getLogger(SendEmailState.class.getName()).log(Level.INFO, "FScheduler is null!");
        }
//        retval = StateFactory.getInstance().createStateComponent(SendJsonState.class.getName());
        return retval;
    }
}
