/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.state;

import com.granberg.model.DelayValue;
import com.granberg.model.News;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import ndxlib.mail.EmailMessage;

/**
 *
 * @author Carlo
 */
public class BuildEmailBodyState extends AbstractState {

    @Override
    public IState next(DataObject dataObject) {
        IState retval = null;
        Logger.getLogger(BuildEmailBodyState.class.getName()).log(Level.INFO, "Build Email Body State called!");
        DelayValue delayValue = dBAccess.findDelayValue(1);
        Calendar cal = Calendar.getInstance();
        cal.setTime(dataObject.getNewsDate());
        cal.add(Calendar.MINUTE, -delayValue.getDelayValue());
        Date sendDate = cal.getTime();
        dataObject.setSendDate(sendDate);
        Date today = new Date();
        if (sendDate.compareTo(today) > 0) {
            List<News> list = dBAccess.findSameDateTimeNews(dataObject.getNewsDate());
            if (list.size() == 0) {
                Logger.getLogger(BuildEmailBodyState.class.getName()).log(Level.INFO, "empty news");
            }
            String currency = "";
            String event = "";
            String forecast = "";
            String previous = "";
            String impact = "";
            for (News news : list) {
                News jsonNews = new News();
                jsonNews.setCurrency(news.getCurrency());
                jsonNews.setEvents(news.getEvents());
                jsonNews.setForecast(news.getForecast());
                jsonNews.setImportance(news.getImportance());
                jsonNews.setPrevious(news.getPrevious());
                jsonNews.setDate(news.getDate());
                dataObject.getNewsList().add(jsonNews);
                forecast = news.getForecast();
                previous = news.getPrevious();
                impact = news.getImportance();
                if (!currency.contains(news.getCurrency())) {
                    currency = currency + news.getCurrency() + ", ";
                }
                if (!event.contains(news.getEvents())) {
                    event = event + news.getEvents() + ", ";
                }
            }
//            String subject = "Upcoming Forex News";
//            dataObject.setSubject(subject);
            String body = String.format("Currencies Affected : %s\n Event Name/s : %s\n Forecast : %s\n Previous : %s\n Importance : %s\n Event Date : %s", currency, event,forecast,previous,impact, dataObject.getNewsDate().toString());
            Logger.getLogger(BuildEmailBodyState.class.getName()).log(Level.INFO, body);
            EmailMessage emailMessage = new EmailMessage();
            emailMessage.setSubject("Upcoming Forex News");
            emailMessage.setBody(body);
            dataObject.setEmailMessage(emailMessage);
//            dataObject.setBody(body);
            retval = StateFactory.getInstance().createStateComponent(GetActiveRecipientsState.class.getName());
        }
        return retval;
    }
}