/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.state;

/**
 *
 * @author Carlo
 */
public interface IState {
    public IState next(DataObject dataObject);
}

