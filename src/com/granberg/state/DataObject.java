/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.state;

import com.granberg.model.News;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Vector;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import ndxlib.mail.EmailMessage;

/**
 *
 * @author Carlo
 */
public class DataObject {

    private static DataObject instance;

    public static DataObject getInstance() {
        if (instance == null) {
            instance = new DataObject();
        }
        return instance;
    }
    private List<Date> distinctNewsDate = null;
    private Vector<ScheduledFuture<?>> ScheduleHandlers;
    private ScheduledExecutorService FScheduler;
    private Date newsDate;
    private EmailMessage emailMessage = null;
    private Date sendDate;
    private String property;
    private News news = null;
    private List<News> newsList = new ArrayList<>();
    private Queue<Object> stack = new LinkedList<>();
    private long sendTimeLapse;

    /**
     * @return the distinctNewsDate
     */
    public List<Date> getDistinctNewsDate() {
        return distinctNewsDate;
    }

    /**
     * @param distinctNewsDate the distinctNewsDate to set
     */
    public void setDistinctNewsDate(List<Date> distinctNewsDate) {
        this.distinctNewsDate = distinctNewsDate;
    }

    /**
     * @return the ScheduleHandlers
     */
    public Vector<ScheduledFuture<?>> getScheduleHandlers() {
        return ScheduleHandlers;
    }

    /**
     * @param ScheduleHandlers the ScheduleHandlers to set
     */
    public void setScheduleHandlers(Vector<ScheduledFuture<?>> ScheduleHandlers) {
        this.ScheduleHandlers = ScheduleHandlers;
    }

    /**
     * @return the FScheduler
     */
    public ScheduledExecutorService getFScheduler() {
        return FScheduler;
    }

    /**
     * @param FScheduler the FScheduler to set
     */
    public void setFScheduler(ScheduledExecutorService FScheduler) {
        this.FScheduler = FScheduler;
    }

    /**
     * @return the newsDate
     */
    public Date getNewsDate() {
        return newsDate;
    }

    /**
     * @param newsDate the newsDate to set
     */
    public void setNewsDate(Date newsDate) {
        this.newsDate = newsDate;
    }

    /**
     * @return the emailMessage
     */
    public EmailMessage getEmailMessage() {
        return emailMessage;
    }

    /**
     * @param emailMessage the emailMessage to set
     */
    public void setEmailMessage(EmailMessage emailMessage) {
        this.emailMessage = emailMessage;
    }

    /**
     * @return the sendDate
     */
    public Date getSendDate() {
        return sendDate;
    }

    /**
     * @param sendDate the sendDate to set
     */
    public void setSendDate(Date sendDate) {
        this.sendDate = sendDate;

    }

    /**
     * @return the property
     */
    public String getProperty() {
        return property;
    }

    /**
     * @param property the property to set
     */
    public void setProperty(String property) {
        this.property = property;
    }

    /**
     * @return the news
     */
    public News getNews() {
        return news;
    }

    /**
     * @param news the news to set
     */
    public void setNews(News news) {
        this.news = news;
    }


    /**
     * @return the stack
     */
    public Queue<Object> getStack() {
        return stack;
    }


    /**
     * @return the sendTimeLapse
     */
    public long getSendTimeLapse() {
        return sendTimeLapse;
    }

    /**
     * @param sendTimeLapse the sendTimeLapse to set
     */
    public void setSendTimeLapse(long sendTimeLapse) {
        this.sendTimeLapse = sendTimeLapse;
    }

    /**
     * @return the newsList
     */
    public List<News> getNewsList() {
        return newsList;
    }

    /**
     * @param newsList the newsList to set
     */
    public void setNewsList(List<News> newsList) {
        this.newsList = newsList;
    }
}